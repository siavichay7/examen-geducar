import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url = environment.url;
  apiKey = environment.apiKey;
  constructor(private _httpClient: HttpClient) { }

  getAlumnos() {
    let headers = new HttpHeaders({'x-api-key': this.apiKey});
    return this._httpClient.get(`${this.url}/alumno`, {headers});
  }

  getAlumno(idAlumno) {
    let headers = new HttpHeaders({'x-api-key': this.apiKey});
    return this._httpClient.get(`${this.url}/alumno/${idAlumno}`, {headers});
  }

  postAlumno(data) {
    let headers = new HttpHeaders({'x-api-key': this.apiKey});
    return this._httpClient.post(`${this.url}/alumno`, data, {headers},);
  }

  updateAlumno(data, idAlumno) {
    let headers = new HttpHeaders({'x-api-key': this.apiKey});
    return this._httpClient.put(`${this.url}/alumno/${idAlumno}`, data, {headers});
  }
}
