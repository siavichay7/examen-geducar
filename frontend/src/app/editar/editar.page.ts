import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Alumno } from '../model/alumno.interface';
import { ApiService } from '../services/api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {

  id: number;
  alumno: Alumno = {};

  constructor(private activatedRoute: ActivatedRoute, 
    private toastController: ToastController,
    private router: Router,
    private loadingController: LoadingController,
    private apiService: ApiService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params.id;
    this.apiService.getAlumno(this.id).subscribe((res:any)=>{
      this.alumno = res.result;
      const FORMATO = 'YYYY-MM-DD';
      const fechaNac = moment( this.alumno.fechaNacimiento,FORMATO);
      this.alumno.fechaNacimiento = fechaNac.format(FORMATO) ;
    });
  }

  async enviar(){
    
    delete this.alumno.idAlumno;
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Por favor espera...',
      duration: 2000
    });
    await loading.present();  
    await loading.onDidDismiss(); 
    const FORMATO = 'YYYY-MM-DD';
    const fechaNac = moment( this.alumno.fechaNacimiento,FORMATO);
    this.alumno.fechaNacimiento = fechaNac.format(FORMATO) ;
    this.apiService.updateAlumno(this.alumno, this.id).subscribe((res:any)=>{
      console.log(res);
      this.mensaje("El alumno se ha guardado correctamente.", "success");
    }, error=>{
      this.mensaje("El alumno no se guardó correctamente.", "danger");
    })
  }

  async mensaje(texto, color) {
    const toast = await this.toastController.create({
      message: texto,
      duration: 2000,
      color: color
    });
    toast.present();
    setTimeout(() => {
      this.router.navigateByUrl('/home');
    }, 2000);
  }



}
