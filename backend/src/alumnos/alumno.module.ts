import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HeaderApiKeyStrategy } from './auth-header-api-key.strategy';
import { AlumnoController } from './controllers/alumno.controller';
import { Alumno } from './entities/alumno.entities';
import { AlumnoService } from './services/alumno.service';

@Module({
    imports: [TypeOrmModule.forFeature([Alumno])],
    controllers: [AlumnoController],
    providers: [AlumnoService, HeaderApiKeyStrategy],
    exports: [AlumnoService],
})
export class AlumnoModule { }
