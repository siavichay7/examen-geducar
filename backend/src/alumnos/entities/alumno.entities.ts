import { Exclude } from 'class-transformer';
import { PrimaryGeneratedColumn, Column, Entity, CreateDateColumn, UpdateDateColumn, OneToOne, ManyToOne, JoinColumn, OneToMany } from 'typeorm';

@Entity({ name: 'alumno' })
export class Alumno {
    @PrimaryGeneratedColumn({name: 'id_alumno'})
    idAlumno: number;

    @Column({ type: 'date',  name: 'fecha_nacimiento'  }) // Recommended
    fechaNacimiento: Date;

    @Column({ type: 'varchar', name: 'nombre_padre' })
    nombrePadre: string;

    @Column({ type: 'varchar', name: 'nombre_madre' })
    nombreMadre: string;

    @Column({ type: 'varchar', name: 'grado' })
    grado: string;

    @Column({ type: 'varchar', name: 'seccion' })
    seccion: string;

    @Exclude()
    @Column({ type: 'datetime', name: 'fecha_ingreso' })
    fechaIngreso: Date;

}
